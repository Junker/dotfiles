version = "0.21.0"
package.path = os.getenv("LUA_PATH") .. ";" .. os.getenv("HOME") .. "/.config/xplr/?/init.lua"
package.cpath = os.getenv("LUA_CPATH") .. ";" .. package.cpath

xplr.config.general.show_hidden = true

-- TEMP!!!!!!!!!!!!
xplr.config.general.disable_debug_error_mode = true

local grp = require "posix.grp"
local pwd = require "posix.pwd"
local grp_cache = {}
local pwd_cache = {}

xplr.fn.custom.j_file_owner = function(m)
	local group = "-"
	local user = "-"

	if grp_cache[m.gid] then
		group = grp_cache[m.gid]
	else
		group = grp.getgrgid(m.gid).gr_name
		grp_cache[m.gid] = group
	end
	if pwd_cache[m.gid] then
		user = pwd_cache[m.uid]
	else
		user = pwd.getpwuid(m.uid).pw_name
		pwd_cache[m.uid] = user
	end
	return user .. ":" .. group
end

local truecolor = os.getenv("COLORTERM") == "truecolor"

-- UI
local function ui_init()
	xplr.config.node_types.symlink.style.fg = "White"

	if truecolor  then
		xplr.config.general.focus_ui.style.bg = "DarkGray"
	else
		xplr.config.general.focus_ui.style.bg = "White"
	end

	xplr.config.general.focus_ui.style.fg = "Black"
	xplr.config.general.focus_ui.prefix = "▸ "
	xplr.config.general.focus_ui.suffix = ""

	xplr.config.general.focus_selection_ui.style.bg = "Yellow"
	xplr.config.general.focus_selection_ui.style.fg = "Black"
	xplr.config.general.focus_selection_ui.prefix = "▸ "
	xplr.config.general.focus_selection_ui.suffix = ""
	xplr.config.general.selection_ui.prefix = "* "
	xplr.config.general.selection_ui.suffix = ""


	xplr.config.general.table.header.cols = {
		{},
	}

	xplr.config.general.table.col_widths = {
		{Percentage = 50},
		{Percentage = 10},
		{Percentage = 10},
		{Percentage = 10},
		{Percentage = 20}
	}

	xplr.config.general.table.row.cols = {
		{
			format = "builtin.fmt_general_table_row_cols_1",
			style = {add_modifiers = nil, bg = nil, fg = nil, sub_modifiers = nil}
		},
		{
			format = "custom.j_file_owner",
			style = {add_modifiers = nil, bg = nil, fg = nil, sub_modifiers = nil}
		},
		{
			format = "builtin.fmt_general_table_row_cols_2",
			style = {add_modifiers = nil, bg = nil, fg = nil, sub_modifiers = nil}
		},
		{
			format = "builtin.fmt_general_table_row_cols_3",
			style = {add_modifiers = nil, bg = nil, fg = nil, sub_modifiers = nil}
		},
		{
			format = "builtin.fmt_general_table_row_cols_4",
			style = {add_modifiers = nil, bg = nil, fg = nil, sub_modifiers = nil}
		}
	}

	-- FIX WRONG COLORS IN ICONS MODULE
	-- xplr.config.node_types.directory.meta.icon = ""
	-- xplr.config.node_types.file.meta.icon = ""
	-- xplr.config.node_types.symlink.meta.icon = ""
end


-- KEYBINDINGS
local function keys_init()
	local key = xplr.config.modes.builtin.default.key_bindings.on_key
	local action_key = xplr.config.modes.builtin.action.key_bindings.on_key

	key.e = action_key.e
	key.c = action_key.c
	key["alt-*"] = action_key.V
	key["f4"] = action_key.e
	key["f2"] = key.r
	key["f8"] = key.d
	key["delete"] = key.d
	key["tab"] = key["right"]
	key["`"] = key["~"];
	key["end"] = key["G"];
	key["home"] = {help = "top", messages = {"FocusFirst", "PopMode"}}
	key["ctrl-t"] = action_key.t

	key["ctrl-w"] = nil
	key["ctrl-i"] = nil
	key["ctrl-o"] = nil
	key["d"] = nil
	key["j"] = nil
	key["h"] = nil
	key["l"] = nil
	key["k"] = nil
	key["."] = nil
	key["G"] = nil
	key["V"] = nil

	key.b = {
		help = "bookmarks",
		messages = {"PopMode", {SwitchModeCustom = "bookmark"}}
	}

	key.v = {
		help = "nuke",
		messages = {"PopMode", {SwitchModeCustom = "nuke"}}
	}
	key["f3"] = xplr.config.modes.custom.nuke.key_bindings.on_key.v
	key["enter"] = xplr.config.modes.custom.nuke.key_bindings.on_key.o

	xplr.config.modes.builtin.delete.key_bindings.on_key["ctrl-d"] = {
		help = "sudo delete",
        messages = {
			{
				BashExec = [===[
              (while IFS= read -r line; do
              if [ -d "$line" ] && [ ! -L "$line" ]; then
                if sudo rmdir -v -- "${line:?}"; then
                  echo LogSuccess: $line deleted >> "${XPLR_PIPE_MSG_IN:?}"
                else
                  echo LogError: Failed to delete $line >> "${XPLR_PIPE_MSG_IN:?}"
                fi
              else
                if sudo rm -v -- "${line:?}"; then
                  echo LogSuccess: $line deleted >> "${XPLR_PIPE_MSG_IN:?}"
                else
                  echo LogError: Failed to delete $line >> "${XPLR_PIPE_MSG_IN:?}"
                fi
              fi
              done < "${XPLR_PIPE_RESULT_OUT:?}")
              echo ExplorePwdAsync >> "${XPLR_PIPE_MSG_IN:?}"
              read -p "[enter to continue]"
            ]===],
			},
			"PopMode",
        },
	}
end

-- INIT XPM
local function xpm_init()
	local home = os.getenv("HOME")
	local xpm_path = home .. "/.local/share/xplr/dtomvan/xpm.xplr"
	local xpm_url = "https://github.com/dtomvan/xpm.xplr"

	package.path = package.path
		.. ";"
		.. xpm_path
		.. "/?.lua;"
		.. xpm_path
		.. "/?/init.lua"

	os.execute(
		string.format(
			"[ -e '%s' ] || git clone '%s' '%s'",
			xpm_path,
			xpm_url,
			xpm_path
		)
	)
end



-- require("bookmarks").setup()

-- require("fileinfo").setup()

xpm_init()
require("xpm").setup({
	plugins = {
		-- Let xpm manage itself
		'dtomvan/xpm.xplr',
		'Junker/nuke.xplr',
		'sayanarijit/fzf.xplr',
		'prncss-xyz/icons.xplr',
		'igorepst/context-switch.xplr',
		'sayanarijit/map.xplr',
		'dtomvan/ouch.xplr',
		{ 'dtomvan/extra-icons.xplr',
			after = function()
				if truecolor then
					xplr.config.general.table.row.cols[2] = { format = "custom.icons_dtomvan_col_1" }
				end
			end
		},
	},
	auto_install = true,
	auto_cleanup = true,
})

require("ouch").setup{
  mode = "action",
  key = "t",
}

require("nuke").setup{
	smart_view = {
		custom = {
			{ extension = "so", command = "ldd -r {} | less"},
		},
	}
}


ui_init()
keys_init()
