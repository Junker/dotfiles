#!/bin/bash

if ! [ -x "$(command -v pacman)" ]
then
	luarocks install luaposix --lua-version 5.1
else
	pacman -S lua51-posix
fi
