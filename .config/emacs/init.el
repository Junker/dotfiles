;; use bash
(setq shell-file-name "/bin/bash")
(defconst explicit-shell-file-name "/bin/bash")
(setenv "SHELL" "/bin/bash")

(make-directory "~/.cache/emacs" t)
(defconst emacs-cache-directory "~/.cache/emacs/")


;; warning if open file larger than 100MB
(setq large-file-warning-threshold 100000000)

;; 200MB for GC
(setq gc-cons-threshold 200000000)

;; collect garbage on idle
(run-with-idle-timer 2 t (lambda () (garbage-collect)))

;; Don't persist a custom file
(setq custom-file (make-temp-file "")) ; use a temp file as a placeholder

;; eln native-comp files
(setq comp-speed 3)  ;optimisation level
(add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache/" emacs-cache-directory))

;; Don't create auto save and lock files. I am only working with 1 emacs instance.
(setq create-lockfiles nil)

;; revert buffers automatically when underlying files are changed externally
(global-auto-revert-mode t)

;; smart tab behavior - indent or complete
(setq tab-always-indent 'complete)

;; nice scrolling
(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 'allways
      mouse-wheel-scroll-amount '(5) ; scroll-step
      mouse-wheel-progressive-speed nil) ;; disable mouse acceleration on scrolling

(setq ring-bell-function 'ignore ; disable the annoying bell ring
      inhibit-startup-screen t  ; disable startup screen
      use-file-dialog nil)


;;setup backup files
(setq make-backup-files nil)
(setq auto-save-default nil)

;; disable ask save password for sudo
(setq auth-sources nil)

;; ============== PACKAGES
(require 'package)
(setq use-package-always-ensure t)
(setq package-user-dir (concat emacs-cache-directory "packages"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; ====================

;; meaningful names for buffers with the same name JJ ???????
(use-package uniquify
  :ensure nil
  :init
  (progn
    (setq uniquify-buffer-name-style 'forward)
    (setq uniquify-separator "/")
    (setq uniquify-after-kill-buffer-p t)    ; rename after killing uniquified
    (setq uniquify-ignore-buffers-re "^\\*"))) ; don't muck with special buffers


;; ediff - don't start another frame
(require 'ediff)
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; clean up obsolete buffers automatically JJJ??
(require 'midnight)

;; (kill-buffer "*scratch*")

(electric-pair-mode)


;; UI
(tool-bar-mode -1)
(menu-bar-mode -1)
(tab-bar-mode 1)
(blink-cursor-mode 1)
(xterm-mouse-mode 1)

;; mode line settings
(line-number-mode t)
(column-number-mode t)
(size-indication-mode t)

(setq next-error-message-highlight t)

(setq-default tab-width 4)

;; enable y/n answers
(fset 'yes-or-no-p 'y-or-n-p)

(use-package no-littering
  :init
  (setq no-littering-etc-directory emacs-cache-directory
        no-littering-var-directory emacs-cache-directory))

(use-package uwu-theme
  :config
  (progn
    (setq uwu-distinct-line-numbers 'nil)
    (load-theme 'uwu t)
    (set-face-attribute 'font-lock-keyword-face nil :slant 'italic)
    (set-face-foreground 'font-lock-warning-face "#131A1C")
    (set-face-background 'font-lock-warning-face "#FF2828")))


(global-display-line-numbers-mode)

;; rainbow-mode
(use-package rainbow-mode
  :defer t)

;; rainbow-delimiters
(use-package rainbow-delimiters
  :defer t
  :hook (racket-mode hy-mode scheme-mode fennel-mode emacs-lisp-mode lisp-mode slime-repl-mode-hook)
  :config
  (custom-set-faces
   '(rainbow-delimiters-depth-1-face ((t (:foreground "#ff44dd")))))) ; make first more visible


(use-package hl-todo
  :config
  (global-hl-todo-mode 1))


;; change cursor line color
(require 'hl-line)
(global-hl-line-mode +1)
(set-face-background 'hl-line "#293024")

(use-package xclip
  :config (xclip-mode 1))


;; ================= ADDITIONAL PACKAGES
;; show available keybindings after you start typing
(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode +1))

(use-package auto-sudoedit
  :config
  (auto-sudoedit-mode 1))

(use-package apheleia
  :defer t)

(use-package whole-line-or-region
  :defer t
  :bind (("M-c" . whole-line-or-region-copy-region-as-kill)
         ("M-x" . whole-line-or-region-kill-region)
         ("C-k" . whole-line-or-region-delete-region)
         ("C-_" . whole-line-or-region-comment-dwim-2)
         ("C-/" . whole-line-or-region-comment-dwim-2)))

(use-package iedit
  :defer t
  :bind (("M-d" . iedit-mode)
         (:map iedit-mode-keymap
          ("<escape>" . iedit--quit))
         (:map iedit-mode-occurrence-keymap
          ("<escape>" . iedit--quit)
          ("C-<up>" . iedit-prev-occurrence)
          ("C-<down>" . iedit-next-occurrence))))

(use-package vertico
  :config (vertico-mode +1))

(use-package consult
  :defer t
  :bind (("C-S-o" . consult-recent-file)
         ("C-r" . consult-imenu)
         ("M-F" . j/files-grep-ripgrep)
         ("C-S-b" . consult-bookmark)
         ("C-<tab>" . consult-buffer)
         ("M-f" . consult-line)
         ("C-~" . consult-complex-command)
         ("M-p" . consult-project-buffer)
         :map isearch-mode-map
         ("M-f" . consult-line))
  :custom ((consult-async-input-debounce 0.1)
           (consult-async-input-throttle 0.2)
           (consult-async-min-input 2))
  :config
  (progn
    (mapcar (lambda (a)
              (add-to-list 'consult-buffer-filter a))
            '("^\\@" "^\\*"))
    (setq register-preview-delay 0.5
          register-preview-function #'consult-register-format)
    (advice-add #'register-preview :override #'consult-register-window)))

(use-package orderless
  :custom
  (orderless-matching-styles '(orderless-literal
                               orderless-prefixes
                               orderless-initialism
                               orderless-regexp))
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)    ; I want to be in control!
  (completion-category-overrides '((file (styles . (basic partial-completion))))))

(use-package expreg
  :defer t
  :bind (("M-a" . expreg-expand)))


(use-package winum
  :bind (("M-0" . winum-select-window-0-or-10)
         ("M-1" . winum-select-window-1)
         ("M-2" . winum-select-window-2)
         ("M-3" . winum-select-window-3)
         ("M-4" . winum-select-window-4)
         ("M-5" . winum-select-window-5)
         ("M-6" . winum-select-window-6)
         ("M-7" . winum-select-window-7)
         ("M-8" . winum-select-window-8)
         ("M-9" . winum-select-window-9))
  :config
  (progn
	  (setq winum-auto-assign-0-to-minibuffer nil
		      winum-auto-setup-mode-line nil
		      winum-ignored-buffers '(" *which-key*"))
    (winum-mode 1)))

(use-package treesit-auto
  ;; :defer t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package markdown-mode
  :defer t)

(use-package nginx-mode
  :defer t)

(use-package crontab-mode
  :defer t)

;; ================= FUNCS
(defun j/delete-sexp (arg)
  (interactive "P")
  (delete-region (point)
                 (progn (forward-sexp arg) (point))))

(defun j/backward-delete-sexp (arg)
  (interactive "P")
  (delete-region (point)
                 (progn (backward-sexp arg) (point))))

(defun j/backward-delete-word (arg)
  (interactive "p")
  (delete-region (point) (progn (backward-word arg) (point))))

(defun j/delete-word (arg)
  (interactive "p")
  (delete-region (point) (progn (forward-word arg) (point))))

;; =============== KEYBINDINGS
;; CUA
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(setq cua-remap-control-z nil)        ;;disable CUA ctrl-z
(setq cua-keep-region-after-copy nil)
(transient-mark-mode 1)               ;; No region when it is not highlighted
(cua-mode t)

(require 'bind-key)
(global-unset-key (kbd "C-h"))
(bind-keys* ("M-`" . execute-extended-command)
	    ("C-q" . save-buffers-kill-emacs)
            ("C-z" . undo)
            ("C-S-z" . undo-redo)
            ("C-y" . undo-redo)
            ("C-a" . mark-whole-buffer) ; ctrl-a select all
            ("C-\\" . 'recenter)
            ("C-j" . goto-line-preview)        ; goto to line number
            ("C-s" . save-buffer)
            ("C-S-s" . write-file) ; Save As. was nil
            ("C-c h" . query-replace)
            ("C-c C-h" . query-replace-regex)
            ;; delete hotkeys
            ("<C-delete>" . j/delete-word)
            ("C-h" . j/backward-delete-word) ; hack: C-h is C-delete in terminal
            ;; "<C-S-delete>" 'j/delete-line
            ;; "<C-S-backspace>" 'j/backward-delete-line
            ("<M-delete>" . j/delete-sexp)
            ("<M-backspace>" . j/backward-delete-sexp)
            ("<M-DEL>" . j/backward-delete-sexp)
            ;;
            ("<f11>" . display-line-numbers-mode)  ; display-line-numbers-mode toggle
            ("<escape>" . keyboard-escape-quit)  ; rebind esc button
            ;; "C-n" 'j/new-empty-buffer  ; new file
            ("C-w" . kill-this-buffer) ; close file
            ;;
            ("M-u" . upcase-region)
            ("C-o" . find-file)
            ("M-j" . xref-find-definitions)
            ("C-S-<iso-lefttab>" . electric-buffer-list)  ;; show all buffers
            ("<f2>" . toggle-truncate-lines) ;;wrap/unwrap lines
            ;; search
            ("C-f" . isearch-forward)
            ("<f3>" . isearch-repeat-forward)
            ("<S-f3>" . isearch-repeat-backward)
            ("<mouse-4>" . (lambda () (interactive) (scroll-down 3)))
            ("<mouse-5>" . (lambda () (interactive) (scroll-up 3))))

