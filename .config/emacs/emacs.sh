#!/usr/bin/env sh
emacs --no-site-file --no-site-lisp --no-x-resources --no-splash -nw $*
