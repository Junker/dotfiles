#!/bin/bash

CG=`tput setaf 2`
CLB=`tput setaf 81`
N="\x1b[0m"

sed -r "s|(https?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]*)|${CG}\1${N}|g" | \
sed "s/^>\+.*/${CLB}&${N}/" <&0
